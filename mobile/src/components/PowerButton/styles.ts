import { StyleSheet } from 'react-native';
import { Colors } from '../../theme/Colors';


export const S = StyleSheet.create({
  powerButton: {
    backgroundColor: Colors.primary,
    borderRadius: 50
  },
  powerIconContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
  }
});
